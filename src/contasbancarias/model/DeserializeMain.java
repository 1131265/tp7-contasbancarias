package contasbancarias.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedro
 */
public class DeserializeMain {
    public static Banco Deserialize() {
        Banco banco = null;
        
        try { 
            FileInputStream fileIn = new FileInputStream("contas.bin");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            banco = (Banco) in.readObject();
            in.close(); fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Classe do Banco não foi encontrada");
            c.printStackTrace();
        }
        
        return banco;
    }
}
